[![Build status](https://ci.appveyor.com/api/projects/status/6ns3esyaadwvy2nt?svg=true)](https://ci.appveyor.com/project/Oxore/placeholder)

# Build steps

## Linux

```
cmake -G Ninja -Bcmake-build-debug . -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=1
cmake --build cmake-build-debug
```

Run:

```
./cmake-build-debug/main
```

Run with leak sanitizer suppressions:

```
LSAN_OPTIONS=suppressions=sanitizer-suppressions.txt ./cmake-build-debug/main
```

Run tests:

```
./cmake-build-debug/test
```

## Windows

The following commands are implied to be executed in powershell. `cmake` and
`msbuild` must be installed and available in the directories, explicitly
specified in PATH environment variable. Tested with Visual Studio 2019
Community Edition installed.

```
cmake -Bcmake-build-debug . -DCMAKE_BUILD_TYPE=Debug
msbuild .\cmake-build-debug\main.vcxproj
msbuild .\cmake-build-debug\test.vcxproj
```

Run:

```
.\cmake-build-debug\Debug\main.exe
```

Run tests:

```
.\cmake-build-debug\Debug\test,exe
```

## Roadmap

+ Integrate libfreetype2
- Implement a little example of GLFW controls
- Integrate any common image loader for textures
- Integrate any common textured 3D models format
