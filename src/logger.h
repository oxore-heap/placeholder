#include <stdarg.h>

// Some colors into your life
#define C_RESET         "\033[0m"
#define C_BLACK         "\033[30m"
#define C_RED           "\033[31m"
#define C_GREEN         "\033[32m"
#define C_YELLOW        "\033[33m"
#define C_BLUE          "\033[34m"
#define C_MAGENTA       "\033[35m"
#define C_CYAN          "\033[36m"
#define C_WHITE         "\033[37m"
#define C_BOLD          "\033[1m"
#define C_BOLDBLACK     "\033[1m\033[30m"
#define C_BOLDRED       "\033[1m\033[31m"
#define C_BOLDGREEN     "\033[1m\033[32m"
#define C_BOLDYELLOW    "\033[1m\033[33m"
#define C_BOLDBLUE      "\033[1m\033[34m"
#define C_BOLDMAGENTA   "\033[1m\033[35m"
#define C_BOLDCYAN      "\033[1m\033[36m"
#define C_BOLDWHITE     "\033[1m\033[37m"

#if defined(__clang__) || defined(__GNUC__) || defined(__GNUG__)
# define LOG_ATTRIBUTE_FORMAT_PRINTF __attribute__ ((format (printf, 1, 2)))
#else
# define LOG_ATTRIBUTE_FORMAT_PRINTF
#endif

LOG_ATTRIBUTE_FORMAT_PRINTF
static inline void error(const char* format, ...)
{
  va_list argp;
  va_start(argp, format);
  fprintf(stderr, C_BOLDRED "Error: " C_RESET);
  vfprintf(stderr, format, argp);
  fprintf(stderr, "\n");
  va_end(argp);
}

LOG_ATTRIBUTE_FORMAT_PRINTF
static inline void warn(const char* format, ...)
{
  va_list argp;
  va_start(argp, format);
  fprintf(stderr, C_BOLDYELLOW "Warn: " C_RESET);
  vfprintf(stderr, format, argp);
  fprintf(stderr, "\n");
  va_end(argp);
}

LOG_ATTRIBUTE_FORMAT_PRINTF
static inline void info(const char* format, ...)
{
  va_list argp;
  va_start(argp, format);
  fprintf(stderr, C_BOLDGREEN "Info: " C_RESET);
  vfprintf(stderr, format, argp);
  fprintf(stderr, "\n");
  va_end(argp);
}

LOG_ATTRIBUTE_FORMAT_PRINTF
static inline void debug(const char* format, ...)
{
  va_list argp;
  va_start(argp, format);
  fprintf(stderr, C_BOLDBLUE "Debug: " C_RESET);
  vfprintf(stderr, format, argp);
  fprintf(stderr, "\n");
  va_end(argp);
}

LOG_ATTRIBUTE_FORMAT_PRINTF
static inline void trace(const char* format, ...)
{
  va_list argp;
  va_start(argp, format);
  fprintf(stderr, C_BOLD "Trace: " C_RESET);
  vfprintf(stderr, format, argp);
  fprintf(stderr, "\n");
  va_end(argp);
}
