#include "placeholder.hpp"

#include "glad/glad.h"
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <cstring>
#include "linmath.h"
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cerrno>
#include <memory>
#include <unordered_map>
#include <string>

#include <logger.h>

/****************************************************************************
 * Function prototypes
 ****************************************************************************/

static std::unique_ptr<char[]> text_file_load(const char *filename);

static void error_callback(int error, const char* description);

static void key_callback(
        GLFWwindow* window, int key, int scancode, int action, int mods);

static void framebuffer_size_callback(
        GLFWwindow* window, int width, int height);

void debug_print_ft_bitmap_mono(FT_Bitmap& bitmap);

void debug_print_ft_bitmap_gray(FT_Bitmap& bitmap);

void debug_print_expanded_bitmap(
        const uint8_t* bitmap, size_t width, size_t height);

GLenum glCheckError_(const char *file, int line);

#define glCheckError() glCheckError_(__FILE__, __LINE__)

/****************************************************************************
 * Data types
 ****************************************************************************/

class NonCopyable
{
public:
    NonCopyable(const NonCopyable&) = delete;
    constexpr NonCopyable& operator=(const NonCopyable& other) = delete;
    NonCopyable(void) {};
};

struct UIntVec2 {
    unsigned x, y;
};

struct IntVec2 {
    int x, y;
};

struct Vec3 {
    float x, y, z;
};

struct Vec2 {
    float x, y;
};

class VertexArray
{
public:
    enum class Type {
        Unknown = -1,
        Fragment = GL_FRAGMENT_SHADER,
        Vertex = GL_VERTEX_SHADER,
    };

    VertexArray(VertexArray&& other)
    {
        *this = std::move(other);
    }

    constexpr VertexArray& operator=(VertexArray&& other)
    {
        this->m_ok = other.m_ok;
        this->m_id = other.m_id;
        other.m_ok = false;
        other.m_id = (GLuint)-1;
        return *this;
    }

    ~VertexArray(void)
    {
        if (m_ok) {
            trace("Destroying VAO id=%u", m_id);
            glDeleteVertexArrays(1, &m_id);
            trace("VAO id=%u destroyed", m_id);
            m_ok = false;
            m_id = (GLuint)-1;
        }
    }

    bool IsOk(void) const { return m_ok; }

    GLuint Id(void) const { return m_id; }

    static VertexArray GenSingle()
    {
        GLuint id;
        glGetError();
        glGenVertexArrays(1, &id);
        bool success = (GL_NO_ERROR == glGetError());
        return VertexArray(id, success);
    }

private:
    VertexArray(GLuint id, bool ok) : m_id(id), m_ok(ok)
    {
        trace("Created valid VAO id=%u", m_id);
    }

    GLuint m_id = (GLuint)-1;
    bool m_ok = false;
};

class VertexBuffer
{
public:
    enum class Type {
        Unknown = -1,
        Fragment = GL_FRAGMENT_SHADER,
        Vertex = GL_VERTEX_SHADER,
    };

    VertexBuffer(VertexBuffer&& other)
    {
        *this = std::move(other);
    }

    constexpr VertexBuffer& operator=(VertexBuffer&& other)
    {
        this->m_ok = other.m_ok;
        this->m_id = other.m_id;
        other.m_ok = false;
        other.m_id = (GLuint)-1;
        return *this;
    }

    ~VertexBuffer(void)
    {
        if (m_ok) {
            trace("Destroying VBO id=%u", m_id);
            glDeleteBuffers(1, &m_id);
            trace("VBO id=%u destroyed", m_id);
            m_ok = false;
            m_id = (GLuint)-1;
        }
    }

    bool IsOk(void) const { return m_ok; }

    GLuint Id(void) const { return m_id; }

    static VertexBuffer GenSingle()
    {
        GLuint id;
        glGetError();
        glGenBuffers(1, &id);
        bool success = (GL_NO_ERROR == glGetError());
        return VertexBuffer(id, success);
    }

private:
    VertexBuffer(GLuint id, bool ok) : m_id(id), m_ok(ok)
    {
        trace("Created valid VBO id=%u", m_id);
    }

    GLuint m_id = (GLuint)-1;
    bool m_ok = false;
};

class Character: public NonCopyable
{
public:
    Character(Character&& other)
    {
        *this = std::move(other);
    }

    Character& operator=(Character&& other)
    {
        this->~Character();
        this->m_ok = other.m_ok;
        this->m_texture_id = other.m_texture_id;
        this->m_size = other.m_size;
        this->m_bearing = other.m_bearing;
        this->m_advance = other.m_advance;
        other.m_ok = false;
        other.m_texture_id = 0;
        other.m_size = {0, 0};
        other.m_bearing = {0, 0};
        other.m_advance = 0;
        return *this;
    }

    ~Character(void)
    {
        if (m_ok)
        {
            trace("Destroying Character object texture_id=%d", m_texture_id);
            glDeleteTextures(1, &m_texture_id);
            m_ok = false;
        }
    }

    bool IsOk(void) const { return m_ok; }

    unsigned TextureId(void) const { return m_texture_id; }

    UIntVec2 Size(void) const { return m_size; }

    IntVec2 Bearing(void) const { return m_bearing; }

    unsigned Advance(void) const { return m_advance; }

    static Character FromFace(const FT_Face face, char index)
    {
        return Character(face, index);
    }

private:
    Character(const FT_Face face, char index)
    {
        // load character glyph
        if (FT_Load_Char(face, index, FT_LOAD_RENDER))
        {
            debug("Character: FreetypeFace failed to load glyph index=%d",
                    index);
            return;
        }

        glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // disable byte-alignment restriction

        // now store character for later use
        m_bearing = {face->glyph->bitmap_left, face->glyph->bitmap_top};
        m_advance = face->glyph->advance.x;
        size_t width, height;
        auto* bitmap = expandBitmap(width, height, face->glyph->bitmap);
        m_size.x = width;
        m_size.y = height;
        if (bitmap == nullptr)
        {
            debug("Character: buffer allocation failed for glyph index=%d",
                    index);
            return;
        }

        // generate texture
        glGenTextures(1, &m_texture_id);
        glBindTexture(GL_TEXTURE_2D, m_texture_id);
        // set texture
        glGetError();
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RED,
            m_size.x,
            m_size.y,
            0,
            GL_RED,
            GL_UNSIGNED_BYTE,
            bitmap);
        glCheckError();
        delete[] bitmap;
        // set texture options
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        // set valid
        m_ok = true;

        trace(
                "Created valid Character object, "
                "texture_id=%u, size=%u:%u, bearing=%d:%d, advance=%u",
                m_texture_id,
                m_size.x,
                m_size.y,
                m_bearing.x,
                m_bearing.y,
                m_advance);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    static size_t upToPowerOf2(size_t a)
    {
        size_t result = 1;
        while (result < a) result *= 2;
        return result;
    }

    static uint8_t* expandBitmap(
            size_t& width,
            size_t& height,
            FT_Bitmap &bitmap)
    {
        width = upToPowerOf2(bitmap.width);
        height = upToPowerOf2(bitmap.rows);
        auto* expanded_data = new uint8_t[width * height];
        if (expanded_data == nullptr)
        {
            return nullptr;
        }

        auto pixel_mode = (FT_Pixel_Mode)bitmap.pixel_mode;
        if (pixel_mode == FT_PIXEL_MODE_MONO)
        {
            expandBitmapMono(width, height, expanded_data, bitmap);
        }
        else if (pixel_mode == FT_PIXEL_MODE_GRAY)
        {
            expandBitmapGray(width, height, expanded_data, bitmap);
        }
        else
        {
            error("Unsupported pixel_mode=%d", pixel_mode);
            return nullptr;
        }
        return expanded_data;
    }

    static void expandBitmapMono(
            size_t& width,
            size_t& height,
            uint8_t* expanded_data,
            FT_Bitmap &bitmap)
    {
        debug_print_ft_bitmap_mono(bitmap);
        for (size_t j = 0; j < height; j++) {
            for (size_t i = 0; i < width/8; i++) {
                uint8_t byte = bitmap.buffer[i + j * (bitmap.width/8)];
                size_t offset = i + j * width;
                if (i < bitmap.width/8 || j < bitmap.rows)
                {
                    expanded_data[offset + 0] = ((byte >> 7) & 1) ? 0xFF : 0;
                    expanded_data[offset + 1] = ((byte >> 6) & 1) ? 0xFF : 0;
                    expanded_data[offset + 2] = ((byte >> 5) & 1) ? 0xFF : 0;
                    expanded_data[offset + 3] = ((byte >> 4) & 1) ? 0xFF : 0;
                    expanded_data[offset + 4] = ((byte >> 3) & 1) ? 0xFF : 0;
                    expanded_data[offset + 5] = ((byte >> 2) & 1) ? 0xFF : 0;
                    expanded_data[offset + 6] = ((byte >> 1) & 1) ? 0xFF : 0;
                    expanded_data[offset + 7] = ((byte >> 0) & 1) ? 0xFF : 0;
                }
                else
                {
                    memset(expanded_data + offset, 0, 8);
                }
            }
        }
        debug_print_expanded_bitmap(expanded_data, width, height);
    }

    static void expandBitmapGray(
            size_t& width,
            size_t& height,
            uint8_t* expanded_data,
            FT_Bitmap &bitmap)
    {
        debug_print_ft_bitmap_gray(bitmap);
        for (size_t j = 0; j < height; j++) {
            for (size_t i = 0; i < width; i++) {
                size_t offset = i + j * width;
                if (i < bitmap.width && j < bitmap.rows)
                {
                    expanded_data[offset] = bitmap.buffer[i + j * bitmap.width];
                }
                else
                {
                    expanded_data[offset] = 0;
                }
            }
        }
        debug_print_expanded_bitmap(expanded_data, width, height);
    }

    bool m_ok{false};
    unsigned m_texture_id{}; // OpenGL ID handle of the glyph texture
    UIntVec2 m_size{};       // Size of glyph: W, H
    IntVec2 m_bearing{};     // Offset from baseline to left/top of glyph: X, Y
    unsigned m_advance{};    // Offset to advance to next glyph
};

using CharacterMap = std::unordered_map<size_t, Character>;

class FreetypeFace : public NonCopyable
{
public:
    FreetypeFace(void)
    {
        trace("Created invalid FreetypeFace object %p", m_face);
    }

    FreetypeFace(FreetypeFace&& other)
    {
        *this = std::move(other);
    }

    FreetypeFace& operator=(FreetypeFace&& other)
    {
        this->~FreetypeFace();
        this->m_ok = other.m_ok;
        this->m_face = other.m_face;
        other.m_ok = false;
        return *this;
    }

    ~FreetypeFace(void)
    {
        if (m_ok) {
            trace("Destroying FreetypeFace object %p", m_face);
            FT_Done_Face(m_face);
            m_ok = false;
        }
    }

    static FreetypeFace FromFile(
            const FT_Library lib, const char *font_path, long font_index)
    {
        FT_Face face{};
        bool ok = !FT_New_Face(lib, font_path, font_index, &face);
        if (ok == false)
        {
            debug("FreetypeFace: Failed to load font\n");
        }

        // set size to load glyphs as
        FT_Set_Pixel_Sizes(face, 0, 48);

        return FreetypeFace(face, ok);
    }

    CharacterMap CreateCharacterMap(void);

    bool IsOk(void) const { return m_ok; }

private:
    FT_Face m_face{};
    bool m_ok{false};

    FreetypeFace(FT_Face face, bool ok) : m_face(face), m_ok(ok)
    {
        trace("Created valid FreetypeFace object %p", m_face);
    }
};

class Freetype : public NonCopyable
{
public:
    Freetype(Freetype&& other)
    {
        *this = std::move(other);
    }

    Freetype& operator=(Freetype&& other)
    {
        this->~Freetype();
        this->m_ok = other.m_ok;
        this->m_ft = other.m_ft;
        other.m_ok = false;
        return *this;
    }

    Freetype(void)
    {
        if (FT_Init_FreeType(&m_ft))
        {
            error("FreeType Library init failed\n");
            return;
        }
        trace("Created valid Freetype object %p", m_ft);
        m_ok = true;
    }

    ~Freetype(void)
    {
        if (m_ok) {
            trace("Destroying Freetype library object %p", m_ft);
            FT_Done_FreeType(m_ft);
            m_ok = false;
        }
    }

    bool IsOk(void) const { return m_ok; }

    FreetypeFace CreateFaceFromFile(const char *font_path, long font_index)
    {
        return FreetypeFace::FromFile(m_ft, font_path, font_index);
    }

private:
    FT_Library m_ft{};
    bool m_ok = false;
};

class Glfw : public NonCopyable
{
public:
    ~Glfw(void)
    {
        if (m_window != nullptr)
        {
            trace("Destroying GLFW window");
            glfwDestroyWindow(m_window);
            m_window = nullptr;
            trace("GLFW window destroyed");
        }
        if (m_ok)
        {
            trace("Terminating GLFW");
            glfwTerminate();
            m_ok = false;
            trace("GLFW terminated");
        }
    }

    bool IsOk(void) const { return m_ok; }

    GLFWwindow *Window(void) const { return m_window; }

    static const Glfw& Instance(void)
    {
        static Glfw instance{};
        return instance;
    }

private:
    Glfw(void) {
        glfwSetErrorCallback(error_callback);
        if (glfwInit() != GLFW_TRUE)
        {
            debug("glfwInit failed!");
            return;
        }
        trace("GLFW initialized");

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

        m_window = glfwCreateWindow(800, 600, "OpenGL", NULL, NULL);
        if (m_window == nullptr)
        {
            debug("glfwCreateWindow failed!");
            this->~Glfw();
            return;
        }
        trace("GLFW window %p created", m_window);

        glfwSetKeyCallback(m_window, key_callback);
        glfwSetFramebufferSizeCallback(m_window, framebuffer_size_callback);
        glfwMakeContextCurrent(m_window);
        gladLoadGL();
        glfwSwapInterval(1);

        m_ok = true;
    }

    bool m_ok{false};
    GLFWwindow* m_window{};
};

class Shader : public NonCopyable
{
public:
    enum class Type {
        Unknown = -1,
        Fragment = GL_FRAGMENT_SHADER,
        Vertex = GL_VERTEX_SHADER,
    };

    Shader(Shader&& other)
    {
        *this = std::move(other);
    }

    constexpr Shader& operator=(Shader&& other)
    {
        this->m_ok = other.m_ok;
        this->m_type = other.m_type;
        this->m_id = other.m_id;
        other.m_ok = false;
        other.m_type = Type::Unknown;
        other.m_id = (GLuint)-1;
        return *this;
    }

    ~Shader(void)
    {
        if (m_ok) {
            trace("Destroying shader id=%u", m_id);
            glDeleteShader(m_id);
            trace("%s shader id=%u destroyed", TypeToString(m_type), m_id);
            m_ok = false;
            m_id = (GLuint)-1;
        }
    }


    bool IsOk(void) const { return m_ok; }

    GLuint Id(void) const { return m_id; }

    static Shader FromFile(const char *filename, Type type)
    {
        auto src = text_file_load(filename);
        assert(src.get() != nullptr);
        return Shader::FromString(src.get(), type, filename);
    }

    static Shader FromString(
            const char *src, Type type, const char *filename=nullptr)
    {
        GLint success = GL_FALSE;
        GLchar compilation_log[kCompilationLogBufferSize] = {};

        assert(src != nullptr);

        auto id = glCreateShader(static_cast<int>(type));
        glShaderSource(id, 1, &src, nullptr);
        glCompileShader(id);
        glGetShaderiv(id, GL_COMPILE_STATUS, &success);
        if (success == GL_FALSE)
        {
            glGetShaderInfoLog(
                    id, kCompilationLogBufferSize, 0, compilation_log);
            error("%s shader \"%s\" compilation failed!\n- %s",
                    TypeToString(type), filename, compilation_log);
        }

        trace("%s shader \"%s\" id=%u compilation finished",
                TypeToString(type), filename, id);
        return Shader(id, type, success == GL_TRUE);
    }

private:
    Shader(GLuint id, Type type, bool ok) : m_id(id), m_type(type), m_ok(ok) {}

    GLuint m_id = (GLuint)-1;
    Type m_type = Type::Unknown;
    bool m_ok = false;
    static constexpr size_t kCompilationLogBufferSize = 512;

    static const char *TypeToString(Type type)
    {
        switch (type) {
        case Type::Fragment: return "fragment";
        case Type::Vertex: return "vertex";
        default: break;
        }
        return "<unknown>";
    }
};

class GpuProgram : public NonCopyable
{
public:
    GpuProgram(GpuProgram&& other)
    {
        *this = std::move(other);
    }

    GpuProgram& operator=(GpuProgram&& other)
    {
        this->~GpuProgram();
        this->m_ok = other.m_ok;
        this->m_id = other.m_id;
        other.m_ok = false;
        other.m_id = (GLuint)-1;
        return *this;
    }

    GpuProgram(void) {}

    ~GpuProgram(void)
    {
        if (m_ok) {
            trace("Destroying GPU program id=%u", m_id);
            glDeleteProgram(m_id);
            trace("GPU program id=%u destroyed", m_id);
            m_ok = false;
            m_id = (GLuint)-1;
        }
    }

    bool IsOk(void) const { return m_ok; }

    GLuint Id(void) const { return m_id; }

    static GpuProgram FromShaders(const Shader &vertex, const Shader &fragment)
    {
        GLint success = GL_FALSE;
        GLchar linkage_log[kLinkageLogBufferSize] = {};

        auto program = glCreateProgram();
        glAttachShader(program, vertex.Id());
        glAttachShader(program, fragment.Id());
        glLinkProgram(program);
        glGetProgramiv(program, GL_LINK_STATUS, &success);
        glDetachShader(program, vertex.Id());
        glDetachShader(program, fragment.Id());
        if (success == GL_FALSE)
        {
            glGetProgramInfoLog(
                    program, kLinkageLogBufferSize, 0, linkage_log);
            error("GPU program linkage failed!\n- %s", linkage_log);
            glDeleteProgram(program);
        }

        trace("GPU program id=%u linkage finished", program);
        return GpuProgram(program, success == GL_TRUE);
    }

private:
    GpuProgram(GLuint program, bool ok) : m_id(program), m_ok(ok) {}

    GLuint m_id = (GLuint)-1;
    bool m_ok = false;

    static constexpr size_t kLinkageLogBufferSize = 512;
};

class AppGlobalContext : public NonCopyable
{
public:
    void Viewport(int32_t width, int32_t height)
    {
        m_width = width;
        m_height = height;
    }

    static AppGlobalContext& Instance(void)
    {
        static AppGlobalContext instance{};
        return instance;
    }

private:
    AppGlobalContext(void) {}
    ~AppGlobalContext(void) {}

    int32_t m_height{}, m_width{};
};

class App : public NonCopyable
{
public:
     App(const Glfw& glfw, CharacterMap&& charmap);
    ~App(void);
    int Run(void);
    bool IsOk(void);

private:
    void renderText(
        const GpuProgram& gpu_prog,
        const std::string& text,
        Vec2 pos,
        float scale,
        Vec3 color);
    bool compileShaders(void);

    const Glfw& m_glfw_context;
    VertexBuffer m_vertex_buffer = VertexBuffer::GenSingle();
    VertexBuffer m_glyph_vertex_buffer = VertexBuffer::GenSingle();
    VertexArray m_glyph_vertex_array = VertexArray::GenSingle();
    GpuProgram m_gpu_program{}, m_glyph_gpu_program{};
    CharacterMap m_charmap{};
    bool m_ok{};
};

struct Vertices
{
    float x, y;
    float r, g, b;
};


/****************************************************************************
 * Constants
 ****************************************************************************/

static const Vertices g_vertices[6] = {
    { -0.5f,  0.0f, 1.0f, 1.0f, 1.0f },
    {  0.0f, -0.5f, 1.0f, 0.5f, 0.0f },
    {  0.5f,  0.0f, 1.0f, 0.5f, 0.0f },

    { -0.5f,  0.0f, 1.0f, 0.5f, 0.0f },
    {  0.5f,  0.0f, 1.0f, 1.0f, 1.0f },
    {  0.0f,  0.5f, 1.0f, 0.5f, 0.0f },
};

extern constexpr size_t kCharacterMapSize = 128;


/****************************************************************************
 * Functions
 ****************************************************************************/

void debug_print_ft_bitmap_mono(FT_Bitmap& bitmap)
{
    printf("pixel_mode=%d\n", bitmap.pixel_mode);
    for (size_t j = 0; j < bitmap.rows; j++)
    {
        for (size_t i = 0; i < bitmap.width/8; i++)
        {
            uint8_t byte = bitmap.buffer[i + j * (bitmap.width/8)];
            uint8_t b0 = (byte >> 0) & 1,
                    b1 = (byte >> 1) & 1,
                    b2 = (byte >> 2) & 1,
                    b3 = (byte >> 3) & 1,
                    b4 = (byte >> 4) & 1,
                    b5 = (byte >> 5) & 1,
                    b6 = (byte >> 6) & 1,
                    b7 = (byte >> 7) & 1;
            printf("%d %d %d %d %d %d %d %d ", b7, b6, b5, b4, b3, b2, b1, b0);
        }
        printf("\n");
    }
    printf("\n");
}

void debug_print_ft_bitmap_gray(FT_Bitmap& bitmap)
{
    printf("pixel_mode=%d, num_grays=%d\n", bitmap.pixel_mode, bitmap.num_grays);
    for (size_t j = 0; j < bitmap.rows; j++)
    {
        for (size_t i = 0; i < bitmap.width; i++)
        {
            printf("%02X ", bitmap.buffer[i + j * bitmap.width]);
        }
        printf("\n");
    }
    printf("\n");
}

void debug_print_expanded_bitmap(
        const uint8_t* bitmap, size_t width, size_t height)
{
    printf("Expanded bitmap\n");
    for (size_t j = 0; j < height; j++)
    {
        for (size_t i = 0; i < width; i++)
        {
            printf("%02X ", bitmap[i + j * width]);
        }
        printf("\n");
    }
    printf("\n");
}


GLenum glCheckError_(const char *file, int line)
{
    GLenum errorCode;
    while ((errorCode = glGetError()) != GL_NO_ERROR)
    {
        const char *err_str;
        switch (errorCode)
        {
            case GL_INVALID_ENUM:                  err_str = "INVALID_ENUM"; break;
            case GL_INVALID_VALUE:                 err_str = "INVALID_VALUE"; break;
            case GL_INVALID_OPERATION:             err_str = "INVALID_OPERATION"; break;
            case GL_STACK_OVERFLOW:                err_str = "STACK_OVERFLOW"; break;
            case GL_STACK_UNDERFLOW:               err_str = "STACK_UNDERFLOW"; break;
            case GL_OUT_OF_MEMORY:                 err_str = "OUT_OF_MEMORY"; break;
            case GL_INVALID_FRAMEBUFFER_OPERATION: err_str = "INVALID_FRAMEBUFFER_OPERATION"; break;
        }
        error("%s:%d: %s", file, line, err_str);
        abort();
    }
    return errorCode;
}

static std::unique_ptr<char[]> text_file_load(const char *filename)
{
    FILE *fp;
    long data_size;
    int errcode = 0, ret = 0;
    std::unique_ptr<char[]> data;

    assert(data.get() == nullptr);

    // Open the file

    fp = fopen(filename, "r");
    errcode = errno;
    if (fp == nullptr)
    {
        debug("%s: fopen(\"%s\", \"r\") = %d: %s",
                __func__, filename, errcode, strerror(errcode));
        return data;
    }

    // Find out file length

    ret = fseek(fp, 0L, SEEK_END);
    errcode = errno;
    if (ret == -1)
    {
        debug("%s: fseek(SEEK_END) = %d: %s",
                __func__, errcode, strerror(errcode));
        fclose(fp);
        return data;
    }

    data_size = ftell(fp);
    errcode = errno;
    if (data_size == (long)-1)
    {
        debug("%s: ftell() = %d: %s",
                __func__, errcode, strerror(errcode));
        fclose(fp);
        return data;
    }

    ret = fseek(fp, 0L, SEEK_SET);
    errcode = errno;
    if (ret == -1)
    {
        debug("%s: fseek(0, SEEK_SET) = %d: %s",
                __func__, errcode, strerror(errcode));
        fclose(fp);
        return data;
    }

    // Check file size

    data_size = data_size < 0 ? 0 : data_size;

    if (sizeof(long) > sizeof(size_t) && data_size > (long)SIZE_MAX)
    {
        debug("%s: file is larger than allowed to load (%ld > %ld)",
                __func__, data_size, (long)SIZE_MAX);
        fclose(fp);
        return data;
    }

    trace("%s: loading file \"%s\" of size %ld", __func__, filename, data_size);

    // Allocate necessary space

    data = std::make_unique<char[]>((size_t)data_size + 1);

    // Read file into string

    size_t s_res = fread(data.get(), 1, (size_t)data_size, fp);
    if (s_res != (size_t)data_size)
    {
        errcode = 0;
        if (ferror(fp)) {
            errcode = errno;
            clearerr(fp);
        }
        debug("%s: fread(size=1, nmemb=%zu) = %zu, err=%d: %s",
                __func__, (size_t)data_size, s_res, errcode, strerror(errcode));
        fclose(fp);
        return data;
    }

    // Nul-terminate

    data[(size_t)data_size] = '\0';

    fclose(fp);
    return data;
}

static void error_callback(int error, const char* description)
{
    (void) error;

    fprintf(stderr, "GLFW Reports %s\n", description);
}

static void key_callback(
        GLFWwindow* window, int key, int scancode, int action, int mods)
{
    (void) scancode;
    (void) mods;

    if ((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q) && action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
}

static void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    (void) window;

    glViewport(0, 0, width, height);
}

int main(void) {
    auto& glfw = Glfw::Instance();
    if (!glfw.IsOk())
    {
        error("GLFW initialization failed!");
        return EXIT_FAILURE;
    }

    CharacterMap charmap;
    {
        Freetype ft{};
        if (!ft.IsOk())
        {
            error("Freetype initialization failed!");
            return EXIT_FAILURE;
        }

        auto face = ft.CreateFaceFromFile("fonts/atarist-normal.bdf", 0);
        //auto face = ft.CreateFaceFromFile("fonts/arial.ttf", 0);
        if (!face.IsOk())
        {
            error("FreetypeFace initialization failed!");
            return EXIT_FAILURE;
        }

        charmap = face.CreateCharacterMap();
        if (charmap.size() < kCharacterMapSize) {
            error("CharacterMap initialization failed!");
            return EXIT_FAILURE;
        }
    }

    App app(glfw, std::move(charmap));
    if (!app.IsOk())
    {
        error("App initialization failed!");
        return EXIT_FAILURE;
    }

    // Essentially a blocking call, because there is main app loop inside
    return app.Run();
}

/****************************************************************************
 * Methods
 ****************************************************************************/

CharacterMap FreetypeFace::CreateCharacterMap(void)
{
    CharacterMap characters{};
    for (size_t i = 0; i < kCharacterMapSize; i++) {
        auto character = Character::FromFace(m_face, i);
        if (character.IsOk()) {
            characters.insert(std::pair<size_t, Character>(
                        i, std::move(character)));
        }
    }
    return characters;
}

App::App(const Glfw& glfw, CharacterMap&& charmap)
    : m_glfw_context(glfw), m_charmap(std::move(charmap))
{
    if (!compileShaders())
    {
        error("Shaders compilation or linkage failed");
        return;
    }
    trace("App initialized");
    m_ok = true;
}

App::~App(void) {}

bool App::compileShaders(void)
{
    {
        auto vertex_shader = Shader::FromFile(
                "glyph.vert", Shader::Type::Vertex);
        assert(vertex_shader.IsOk());
        auto fragment_shader = Shader::FromFile(
                "glyph.frag", Shader::Type::Fragment);
        assert(fragment_shader.IsOk());
        m_glyph_gpu_program = GpuProgram::FromShaders(
                vertex_shader, fragment_shader);
        trace(
                "vertex_shader=%d, fragment_shader=%d",
                vertex_shader.Id(),
                fragment_shader.Id());
    }

    {
        auto vertex_shader = Shader::FromFile(
                "default.vert", Shader::Type::Vertex);
        assert(vertex_shader.IsOk());
        auto fragment_shader = Shader::FromFile(
                "default.frag", Shader::Type::Fragment);
        assert(fragment_shader.IsOk());
        trace(
                "vertex_shader=%d, fragment_shader=%d",
                vertex_shader.Id(),
                fragment_shader.Id());
        m_gpu_program = GpuProgram::FromShaders(vertex_shader, fragment_shader);
    }

    return m_gpu_program.IsOk() && m_glyph_gpu_program.IsOk();
}

void App::renderText(
        const GpuProgram& gpu_prog,
        const std::string& text,
        Vec2 pos,
        float scale,
        Vec3 color)
{
    int width, height;
    glfwGetFramebufferSize(m_glfw_context.Window(), &width, &height);

    // Glyph locations
    glUseProgram(gpu_prog.Id());
    GLint projection_location, textColor_location;
    projection_location = glGetUniformLocation(
            gpu_prog.Id(), "projection");
    textColor_location = glGetUniformLocation(
            gpu_prog.Id(), "textColor");

    // Glyph matrices
    mat4x4 projection;
    mat4x4_ortho(projection, 0.f, width, 0.f, height, 0.f, 1.f);
    glUniformMatrix4fv(projection_location, 1, GL_FALSE, (const GLfloat*)projection);

    glUniform3f(textColor_location, color.x, color.y, color.z);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(m_glyph_vertex_array.Id());

    for (auto c: text)
    {
        if ((unsigned char)c > kCharacterMapSize)
        {
            c = 0x20;
        }
        const Character& glyph = m_charmap.at(c);

        const float xpos = pos.x + glyph.Bearing().x * scale;
        const float ypos = pos.y - (glyph.Size().y - glyph.Bearing().y) * scale;

        const float w = glyph.Size().x * scale;
        const float h = glyph.Size().y * scale;
        // update VBO for each character
        const float vertices_l[6][4] = {
            { xpos,     ypos + h,   0.0f, 0.0f },
            { xpos,     ypos,       0.0f, 1.0f },
            { xpos + w, ypos,       1.0f, 1.0f },

            { xpos,     ypos + h,   0.0f, 0.0f },
            { xpos + w, ypos,       1.0f, 1.0f },
            { xpos + w, ypos + h,   1.0f, 0.0f },
        };
        // render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, glyph.TextureId());
        // update content of VBO memory
        glBindBuffer(GL_ARRAY_BUFFER, m_glyph_vertex_buffer.Id());
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices_l), vertices_l); // be sure to use glBufferSubData and not glBufferData

        // render quad
        glDrawArrays(GL_TRIANGLES, 0, 6);

        // now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        pos.x += glyph.Advance()/64 * scale;
    }
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glUseProgram(0);
}

bool App::IsOk(void) { return m_ok; }

int App::Run(void)
{
    int width, height;

    // configure VAO/VBO for texture quads
    // -----------------------------------
    assert(m_glyph_vertex_array.IsOk());
    assert(m_glyph_vertex_buffer.IsOk());
    glBindVertexArray(m_glyph_vertex_array.Id());
    glBindBuffer(GL_ARRAY_BUFFER, m_glyph_vertex_buffer.Id());
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

    // Face culling may squish result, enable only when text rendering works
    // somehow
    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    while (!glfwWindowShouldClose(m_glfw_context.Window()))
    {
        float ratio;

        glfwGetFramebufferSize(m_glfw_context.Window(), &width, &height);
        ratio = width / (float) height;

        glClear(GL_COLOR_BUFFER_BIT);

        // Draw triangles
        if (1) {
            GLint mvp_location, vpos_location, vcol_location;

            // Triangles locations
            glUseProgram(m_gpu_program.Id());
            mvp_location = glGetUniformLocation(m_gpu_program.Id(), "MVP");
            vpos_location = glGetAttribLocation(m_gpu_program.Id(), "vPos");
            vcol_location = glGetAttribLocation(m_gpu_program.Id(), "vCol");

            // Create buffer and load data for triangle
            glBindBuffer(GL_ARRAY_BUFFER, m_vertex_buffer.Id());
            glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertices), g_vertices, GL_STATIC_DRAW);

            // Triangles matrices
            mat4x4 m, p, mvp;
            mat4x4_identity(m);
            mat4x4_rotate_Z(m, m, (float) glfwGetTime());
            mat4x4_ortho(p, -ratio, ratio, -1.f, 1.f, 1.f, -1.f);
            mat4x4_mul(mvp, p, m);

            // Specify layout of data in m_vertex_buffer for vPos and vCol
            glEnableVertexAttribArray(vpos_location);
            glVertexAttribPointer(vpos_location, 2, GL_FLOAT, GL_FALSE,
                                  sizeof(g_vertices[0]), (void*) 0);
            glEnableVertexAttribArray(vcol_location);
            glVertexAttribPointer(vcol_location, 3, GL_FLOAT, GL_FALSE,
                                  sizeof(g_vertices[0]), (void*) (sizeof(float) * 2));

            glUniformMatrix4fv(mvp_location, 1, GL_FALSE, (const GLfloat*) mvp);
            glDrawArrays(GL_TRIANGLES, 0, 6);
        }

        renderText(
                m_glyph_gpu_program,
                std::string("Nice little letters! :3"),
                Vec2{50.f, 50.f},
                1.f,
                Vec3{0, 0, 0});

        glfwSwapBuffers(m_glfw_context.Window());
        glfwPollEvents();
    }

    return EXIT_SUCCESS;
}
